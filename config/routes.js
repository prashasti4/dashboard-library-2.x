app.config(function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    $httpProvider.defaults.withCredentials = true;
    delete $httpProvider.defaults.headers.common["X-Requested-With"];
    $httpProvider.defaults.headers.common["Accept"] = "application/json";
    $httpProvider.defaults.headers.common["Content-Type"] = "application/json";
})

try{
app.config(['$stateProvider', '$urlRouterProvider', '$logProvider',
    function($stateProvider, $urlRouterProvider) {
//important: for setting default view, first give url of parent then slash'/'than state name of template which you want as default.
   
        $stateProvider
            .state('home', {
                url: '/',
                views: {

                    '@': {
                        templateUrl: './views/home.view.html',
                        controller: 'HomeCtrl as vm'
                    }
                }
            })
            .state('home.charts', {
                url: 'charts',
                abstract: true
            })
            .state('home.charts.lineChart', {
                url: '/home.charts.lineChart',
                views: {
                    'content@home': {
                        templateUrl: "./views/lineChart.html",
                        controller: "ChartCtrl"
                    },

                }
            })
            .state('home.charts.barChart', {
                url: "/home.charts.barChart",
                views: {
                    'content@home': {
                        templateUrl: "./views/barChart.html",
                        controller: "ChartCtrl"
                    }
                }
            })
            .state('home.charts.bubbleChart', {
                url: '/home.charts.bubbleChart',

                views: {

                    'content@home': {
                        templateUrl: "./views/bubbleChart.html",
                        controller: "ChartCtrl"
                    }
                }
            })
            .state('home.charts.doughnutChart', {
                url: '/home.charts.doughnutChart',

                views: {
                    'content@home': {
                        templateUrl: "./views/doughnutChart.html",
                        controller: "ChartCtrl"
                    }
                }
            })

        .state('home.charts.dynamicChart', {
            url: '/home.charts.dynamicChart',

            views: {
                'content@home': {
                    templateUrl: "./views/dynamicChart.html",
                    controller: "ChartCtrl"
                }
            }
        })

        .state('home.charts.horizontalBarChart', {
            url: '/home.charts.horizontalBarChart',

            views: {
                'content@home': {
                    templateUrl: "./views/horizontalBarChart.html",
                    controller: "ChartCtrl"
                }
            }
        })

        .state('home.charts.mixedTypeChart', {
            url: '/home.charts.mixedTypeChart',

            views: {
                'content@home': {
                    templateUrl: "./views/mixedTypeCharts.html",
                    controller: "ChartCtrl"
                }
            }
        })

        .state('home.charts.pieChart', {
            url: '/home.charts.pieChart',

            views: {
                'content@home': {
                    templateUrl: "./views/pieChart.html",
                    controller: "ChartCtrl"
                }
            }
        })

        .state('home.charts.polarAreaChart', {
            url: '/home.charts.polarAreaChart',

            views: {
                'content@home': {
                    templateUrl: "./views/polarAreaChart.html",
                    controller: "ChartCtrl"
                }
            }
        })

        .state('home.charts.radarChart', {
            url: '/home.charts.radarChart',

            views: {
                'content@home': {
                    templateUrl: "./views/radarChart.html",
                    controller: "ChartCtrl"
                }
            }
        })


        .state('home.charts.overview', {
            url: '/home.charts.overview',
            views: {

                'content@home': {
                    templateUrl: "./views/overview.html",
                    //controller: 'HomeCtrl as vm'
                }
            }
        })

        .state('home.grids',{
            url:'grids',
            abstract: true
        })

        .state('home.grids.basicview',{
            url:'/home.grids.basicview',
            views:{
                'content@home':{
                    templateUrl:"./views/basicGrid.html",
                    //controller:
                }
            }
        })

         .state('home.grids.filter',{
            url:'/home.grids.filter',
            views:{
                'content@home':{
                    templateUrl:"./views/filterGrid.html",
                    //controller:
                }
            }
        })

         .state('home.grids.pagination',{
            url:'/home.grids.pagination',
            views:{
                'content@home':{
                    templateUrl:"./views/paginationGrid.html",
                    //controller:
                }
            }
        })

          $urlRouterProvider.otherwise("/charts/home.charts.overview");
    }
])
}catch(exception){
    alert("Exception: "+exception);
}

login.config(['$stateProvider', '$urlRouterProvider', '$logProvider',
    function($stateProvider, $urlRouterProvider) {
        try {
            $urlRouterProvider.otherwise("/");
            $stateProvider
                .state('login', {
                    url: '/',

                    views: {

                        '@': {
                            templateUrl: 'index',
                        }
                    }
                })


            //$routeProvider.when('/loggedIn', { templateUrl: './views/home.view.html', reloadOnSearch: false });

        } catch (exception) {
            console.log('Routing failed');
            alert("Exception: "+exception);
        }
    }
]);


//abstract: true indicates but you want to set this since you'd never go to this state directly, you'd 
//always go to one of it's child states.
