try {
    app.controller('FilterController', ['$scope', '$rootScope', 'ngPopoverFactory', '$http', '$attrs', 'isMobileFilter', 'getFilterDataService', 'uiGridConstants', 'filterCharts', 'chartFilterParameters', function($scope, $rootScope, ngPopoverFactory, $http, $attrs, isMobileFilter, getFilterDataService, uiGridConstants, filterCharts, chartFilterParameters) {
        //render html only when http call completes
        $scope.locationReady = false;
        $scope.doctorReady = false;
        //variable declaration
        $scope.locations = [];
        $scope.doctors = [];
        $scope.model = {};
        $scope.normalMultiselectModel = [];
        $scope.lag = { lessthan: "", greaterthan: "" };
        //object to store selected filter parameters
        $rootScope.filterParameter = { location: "", doctor: "", lagdays: "-1", maxlagdays: "-1", dos_doe: "1", dateRange: "" };
        $scope.multiselectFeatures = [
            { value: "Custom Drop Directions" },
            { value: "Selection Callback" },
            { value: "Dropdown Open Callback" },
            { value: "Dropdown Close Callback" },
            { value: "Limit Max Selections" },
            { value: "Custom Class for CSS" },
            { value: "All/None Selection" },
            { value: "Display & Select Different Keys" }
        ];
        $scope.data = { switch: 'Date of Service' };
        $scope.message = 'false';
        $rootScope.gridApi = { api: "" };
        //Grid variable declaration
        $scope.gridOptions = {
            enableFiltering: false,
            enableSorting: false,
            onRegisterApi: function(gridApi) {
                $rootScope.gridApi.api = gridApi;
            }

<<<<<<< HEAD
        };

        //http requests to get data for filter dropdowns
        var location = "-201,-205,-451,201,203,205,260,270,271,279,280,281,282,283,284,290,291,292,293,299,503,509,513,5555,602,809";
        var doctor = "1623,1635,1662,1665,1680,2000,3001,3003,3004,3005,3006,3009,3011,3013,3014,3020,3025,3026,3027,3030,3031,3032,3033,3035,3036,3050,3051,3052,3053,3054,3055,3056,3060,3072,3210,3211,3212,3231,3241,3300,3301,3302,3303,3304,3305,3306,3307,3308,3309,3350,3351,3352,3353,3354,3401,3402,3403,3405,3406,3805,4301,4401,4603,5000,5001,5002,5005";
        var orgname = "ICS";

        $scope.getFilterData = function() {
            //get locations
            getFilterDataService.async("getLocationforDropDown", location, doctor, orgname).then(function(response) {
                $scope.locations = response;
                $scope.locationReady = true;
=======
        $scope.model = { locations: "" };
        $scope.normalMultiselectModel = [];
        $scope.lag = { lessthan: "", greaterthan: "" };
        $scope.btnText="ALL LOCATIONS";
        try {
            $scope.goButton = function() {
                alert("Submit filters");
                var locations = document.getElementById('material').innerHTML;

                //alert($scope.data.switch);
                var doctors = document.getElementById('material2').innerHTML;
                $scope.filterParameters = { date: $scope.ctrl.datepicker, locations: locations, doctors: doctors, lag: "5" };
                //alert(angular.toJson($scope.filterParameters));
                url = "http://192.168.61.132/GGCService.asmx/GridBind"
                var config = {
                    method: 'POST',
                    url: '../curlproxy.php',
                    data: {
                        requester: "dynamicCtrl",
                        requestedUrl: url,
                        a: $scope.filterParameters.locations
                    }
                };
                var request = $http(config); //created request
                request.then(function(response) { //call to 'then' function
>>>>>>> a85996b62f92d469633bf8e2bc48c9ab1b49ec62

            });
            //get doctors
            getFilterDataService.async("getDoctorsforDropDown", location, doctor, orgname).then(function(response) {
                $scope.doctors = response;
                $scope.doctorReady = true;

            })
        }

        //Bind Basic Grid
        $scope.gridOptions.columnDefs = [
            { field: 'LocationName', width: 150 },
            { field: 'DoctorName', width: 150 },
            { field: 'TransactionDate', width: 150 },
            { field: 'DOS', width: 150 },
            { field: 'TicketNumber', width: 150 },
            { field: 'Charges', width: 150 },
            { field: 'LagDays', width: 150 }
        ];


        $scope.getGridData = function() {
            var url = 'http://192.168.61.132/Controller.aspx';
            var config = {
                method: 'POST',
                url: '../reportCurl.php',
                data: {
                    requestedUrl: url,
                    Servicetype: "GetServiceByLocation",
                    locationids: $rootScope.filterParameter.location,
                    Doctorids: $rootScope.filterParameter.doctor,
                    fromdate: "",
                    todate: "",
                    LagOperatorType: "",
                    lagdays: $rootScope.filterParameter.lagdays,
                    maxlagdays: $rootScope.filterParameter.maxlagdays,
                    type: 3,
                    reportid: 1,
                    dos_doe: $rootScope.filterParameter.dos_doe,
                    weekenddays: 0,
                    CAutoID: 1,
                    SAutoID: 1,
                    orgname: "ICS",
                    length: 1000,
                    start: 0
                }
            };
            var request = $http(config); //created request
            request.then(function(response) {
                showLoader();
                //alert(angular.toJson(response.data));
                try {
                    if (angular.toJson(response.data) != [])
                        $rootScope.gridApi.api.grid.options.data = response.data;
                    //reseting filter parameters
                    $rootScope.filterParameter.doctor = "";
                } catch (exception) {
                    alert("Exception: " + exception);
                    hideLoader();
                }
                hideLoader();
            }, function(error) {
                console.log(error);
            })
        }

        //getting response from webservice on submission of selected filter parameter 
        $scope.goButton = function() {
            showLoader();
            //checking which view (chart/grid)
            var selectedMenu = filterCharts.section;
            var selectedReportId=chartFilterParameters.reportid;
            alert(selectedReportId);
            if (selectedMenu.includes('home.charts')) {
                //alert(angular.toJson($rootScope.filterParameter));            
                    chartFilterParameters.locationids= $rootScope.filterParameter.location,
                    chartFilterParameters.Doctorids= $rootScope.filterParameter.doctor,
                    chartFilterParameters.fromdate= "",
                    chartFilterParameters.todate= "",
                    chartFilterParameters.LagOperatorType= "",
                    chartFilterParameters.lagdays= $rootScope.filterParameter.lagdays,
                    chartFilterParameters.maxlagdays= $rootScope.filterParameter.maxlagdays,
                    chartFilterParameters.type= 3,
                    chartFilterParameters.reportid= filterCharts.id,
                    chartFilterParameters.dos_doe= $rootScope.filterParameter.dos_doe,
                    chartFilterParameters.weekenddays= 0,
                    chartFilterParameters.CAutoID= 1,
                    chartFilterParameters.SAutoID= 1,
                    chartFilterParameters.orgname= "ICS",
                    chartFilterParameters.length= 1000,
                    chartFilterParameters.start= 0
 
                    $rootScope.$emit("callChartMethod", selectedReportId);

            } else {
                $scope.getGridData();
            }
            hideLoader();
        }

<<<<<<< HEAD
        //Reset function
        $scope.clearFn = function() {

            if (document.getElementById('MYDatepicker').value != "") {
                $scope.ctrl.datepicker = "SELECT PERIOD";
            }

            document.getElementById("selectLagFilter").placeholder = "SELECT LAG";
            chartFilterParameters.locationids= "",
            chartFilterParameters.Doctorids= "",
            chartFilterParameters.fromdate= "",
            chartFilterParameters.todate= "",
            chartFilterParameters.LagOperatorType= "",
            chartFilterParameters.lagdays= "",
            chartFilterParameters.maxlagdays= "",
            chartFilterParameters.type= "",
            chartFilterParameters.reportid= 2,
            chartFilterParameters.dos_doe= "",
            chartFilterParameters.weekenddays= 0,
            chartFilterParameters.CAutoID= 1,
            chartFilterParameters.SAutoID= 1,
            chartFilterParameters.orgname= "ICS",
            chartFilterParameters.length= "",
            chartFilterParameters.start= 0
        }

        //submit selected filters function function
        $scope.go = function(selectedId, triggerId) {

            if (isMobileFilter.isMobileFilter == false && triggerId != "mobLocation" && triggerId != "mobDoctor") {
                if (triggerId == 'location' && selectedId != undefined && $scope.filterParameter.location == "") {
                    $scope.filterParameter.location = selectedId;
                }
                if (triggerId == 'doctor' && selectedId != undefined && $scope.filterParameter.doctor == "") {
                    $scope.filterParameter.doctor = selectedId;

                }


            } else {
                if (isMobileFilter.isMobileFilter == true && (triggerId == "mobLocation" && triggerId != "mobDoctor")) {
                    if (triggerId == 'mobLocation' && selectedId != undefined && $scope.filterParameter.location == "") {
                        $scope.filterParameter.location = selectedId;
                    } else $scope.selectedLocation = "";
                    if (triggerId == 'mobDoctor' && selectedId != undefined && $scope.filterParameter.doctor == "") {
                        $scope.filterParameter.doctor = selectedId;
                    } else $scope.selectedDoctor = "";

                }

            }


        }

        //Display selected data on select lag filter
        $scope.lagchange = function(valueType) {
                if ($scope.lag.greaterthan < $scope.lag.lessthan) {
                    $rootScope.filterParameter.lagdays = $scope.lag.lessthan;
                    $rootScope.filterParameter.maxlagdays = $scope.lag.greaterthan;
                } else {
                    if ($scope.lag.greaterthan != "" && $scope.lag.lessthan != "") {
                        alert("Greater than value can not be bigger than Less than value");
                    }
                }
                if ($scope.lag.lessthan != null && $scope.lag.greaterthan != null) {
                    document.getElementById("selectLagFilter").value = $scope.lag.greaterthan + ", " + $scope.lag.lessthan;
                } else {
                    document.getElementById("selectLagFilter").value = "SELECT LAG";
                }


            }
            //function called on switch change
        $scope.onSwitchChange = function() {
            if ($scope.data.switch == 'Date of Service') {
                $rootScope.filterParameter.dos_doe = 1;
                alert($rootScope.filterParameter.dos_doe)
            } else {
                $rootScope.filterParameter.dos_doe = 0;
                alert($rootScope.filterParameter.dos_doe)
            }
        };
        //set Date range
        $scope.setDateRange = function() {
            $scope.filterParameter.dateRange = $scope.ctrl.datepicker;
        }



=======
    

        $scope.displayLagDiv = function() {
            //alert("lag display");
            var element = document.getElementById('selectLagPopup');
            if (element.style.display !== 'none') {
                angular.element(document.querySelectorAll("#selectLagPopup")).css('display', 'none');
            } else {
                angular.element(document.querySelectorAll("#selectLagPopup")).css('display', 'block');
            }
        }
        $scope.lagchange = function(valueType) {
            if($scope.lag.lessthan!=null && $scope.lag.greaterthan!=null){
              document.getElementById("selectLagFilter").placeholder=$scope.lag.greaterthan+", "+$scope.lag.lessthan;
            }else{
              document.getElementById("selectLagFilter").placeholder="SELECT LAG";
            }

        }

        //Reset function
        $scope.ctrlFn= function() {
        //alert("reset all");
        $scope.ctrl.datepicker="SELECT PERIOD";
        //$scope.btnText="";
        document.getElementById("selectLagFilter").placeholder="SELECT LAG";
                }
>>>>>>> a85996b62f92d469633bf8e2bc48c9ab1b49ec62
    }]);
} catch (exception) {
    alert("Exception " + exception);
}




try {
    app.controller('ReportHeaderCtrl', ['$scope', '$http', function($scope, $http) {
        $scope.getReportHeading = function(reportId) {
            $scope.requester = reportId;
            var config = {
                method: 'POST',
                url: './services/reportHeading.php',
                headers: {
                    'Content-Type': 'application/json'

                },
                data: {
                    reportId: $scope.requester,
                    b: "alskdjf"
                }
            };
            var request = $http(config); //created request
            request.then(function(response) { //call to 'then' function
                $scope.reportTitle = angular.toJson(response.data.reportTitle).replace(/"/g, '');
                $scope.dateRange = angular.toJson(response.data.dateRange).replace(/"/g, '');
            })
        }

        //PDF EXPORT FUNCTIONs
        $scope.generatePdf = function() {
            var config = {
                method: 'POST',
                //url: '../services/pdfproxy.php',
                url: '../proxy.php',
                headers: {
                    'Content-Type': 'application/pdf',
                    'Content-Disposition': 'attachment'
                },
                data: {
                    requester: "detailsGridController",
                    //requestedUrl:"http://192.168.61.224/jsons/detailsTree.txt"
                    requestedUrl: "http://111.118.251.210/tcpdf/examples/example_001.php",
                    footerLogo: 'ggcServerSidePdf/images/icons/Footer_Logo_Web.png',
                    dataUrl: '../../jsons2/detailsTree.json',
                    headerColor: 243,
                    headerTextColor: 119,
                    headerFontSize: 8,
                    rowColor: 212,
                    rowTextColor: 89,
                    rowFontSize: 7,
                    pageOrientation: 'P',
                    headerLogo: 'ggcServerSidePdf/images/icons/GGC_Logo.png',
                    headerTitle: 'PROCEDURE STATISTICS & WRVU BY PHY BY DEPT',
                    tableWidth: 'array(25,25,25,25)'
                }
            };
            var request = $http(config); //created request
            request.then(function(response) { //call to 'then' function
                //alert("got response");
                //console.log(angular.toJson(response.data));
                /*var file = new Blob([response], {type: 'application/pdf'});
                  var fileURL = URL.createObjectURL(file);
                  $scope.content = $sce.trustAsResourceUrl(fileURL);*/
                window.open(response.data);
                // window.open("data:application/pdf," +escape(response.data));
            }, function(error) {
                alert(angular.toJson(error));
            });
        }

    }]);

} catch (exception) {
<<<<<<< HEAD
    alert("Exception: " + exception);
}
=======
    alert(exception);
}
>>>>>>> a85996b62f92d469633bf8e2bc48c9ab1b49ec62
