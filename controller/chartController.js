app.controller('chartController', function($scope) {
    $scope.display = "user";
})

app.controller('AppCtrl', ['$scope', '$mdSidenav', 'isMobileFilter', function($scope, $mdSidenav, isMobileFilter) {

    $scope.toggleSidenav = function(menuId) {
        $mdSidenav(menuId).toggle();

    };
    $scope.showfilter = function() {
        isMobileFilter.isMobileFilter = true;
        console.log(isMobileFilter.isMobileFilter);
        if (window.innerWidth >= 729) {} else {
            $scope.toggleSidenav('right');
        }

    }

}])

app.controller("ChartCtrl", function($scope, $rootScope, $http, httpService, $interval, chartFilterParameters) {
    //checking platform(desktop/mobile) to plot charts accordingly 
    $scope.isWebpage = isWeb();
    if ($scope.isWebpage != true) {
        $scope.showLegend = false;
        $scope.responsive = true;
    } else {
        $scope.showLegend = true;
        $scope.responsive = false;
    }

    //defining chart functions in $on so that can be called on click of Go button
    $rootScope.$on("callChartMethod", function(event, selectedReportId) {
        //alert(angular.toJson(selectedReportId));
        if (selectedReportId == 2) {
            $scope.plotLineChart();
        }
        if (selectedReportId == 3) {
            $scope.plotBarChart();
        }
        if (selectedReportId == 5) {
            $scope.plotBubbleChart();
        }
        if (selectedReportId == 8) {
            $scope.plotDoughnutChart();
        }
        if (selectedReportId == 11) {
            $scope.plotDynamicChart();
        }
        if (selectedReportId == 6) {
            $scope.plotHorizontalBarChart();
        }
        if (selectedReportId == 7) {
            $scope.plotMixedChart();
        }
        if (selectedReportId == 4) {
            $scope.plotPieChart();
        }
        if (selectedReportId == 10) {
            $scope.plotPolarChart();
        }
        if (selectedReportId == 9) {
            $scope.plotRadarChart();
        }
    });


    //charting functions
    $scope.plotLineChart = function() {
        try {
            chartFilterParameters.reportid = 2;
            httpService.async().then(function(response) {
                showLoader();
                $scope.labels = response.data.labels;
                $scope.series = response.data.series;
                $scope.data = response.data.data;
                $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
                $scope.options = {
                    scales: {
                        yAxes: [{
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left',
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringx,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"
                            },
                            ticks: {
                                max: 8000,
                                min: 0,
                                stepSize: 2000,
                                callback: function(value, index, values) {
                                    if (parseInt(value) > 999) {
                                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else if (parseInt(value) < -999) {
                                        return '($' + Math.abs(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                    } else {
                                        return '$' + value;
                                    }
                                }
                            },
                            gridLines: {
                                display: true
                            }
                        }, {
                            id: 'y-axis-2',
                            type: 'linear',
                            display: false,
                            position: 'right'

                        }],
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringy,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"

                            },
                            gridLines: {
                                display: false,
                                drawTicks: true
                            }
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    title: {
                        display: true,
                        text: response.data.title
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777",

                        }
                    }
                };

                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];            

                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoadere();
            });

        } catch (exception) {
            alert("Exception(line): " + exception);
        }
    }


    $scope.plotBarChart = function() {
        try {
            chartFilterParameters.reportid = 3;
            httpService.async().then(function(response) {
                showLoader();
                $scope.labels = response.data.labels;
                $scope.series = response.data.series; //legend
                $scope.data = response.data.data;
                $scope.options = {
                    scales: {
                        yAxes: [{
                            id: 'y-axis-1',
                            type: 'linear',
                            display: true,
                            position: 'left',
                            ticks: {
                                max: 210,
                                min: 0,
                                stepSize: 30,
                                callback: function(value, index, values) {
                                    if (parseInt(value) > 999) {
                                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else if (parseInt(value) < -999) {
                                        return '($' + Math.abs(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                    } else {
                                        return '$' + value;
                                    }
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringx,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"


                            },
                            gridLines: {
                                display: true,
                                drawTicks: true
                            }

                        }, {
                            id: 'y-axis-2',
                            type: 'linear',
                            display: false,
                            position: 'right',

                        }],
                        xAxes: [{
                            display: true,
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringy,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"


                            },
                            gridLines: {
                                display: false,
                                drawTicks:true
                            }
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    title: {
                        display: true,
                        text: response.data.title
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                };
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoadere();
            })

        } catch (exception) {
            alert("Exception(bar): " + exception);
        }
    }


    $scope.plotBubbleChart = function() {
        chartFilterParameters.reportid = 5;
        try {
            showLoader();
            $scope.data =
                $scope.options = {
                    scales: {
                        xAxes: [{
                            display: false,
                            ticks: {
                                max: 80,
                                min: -80,
                                stepSize: 20
                            },
                            gridLines: {
                                display: true
                            }
                        }],
                        yAxes: [{
                            display: true,
                            ticks: {
                                max: 125,
                                min: -125,
                                stepSize: 50
                            },
                            gridLines: {
                                display: true

                            }
                        }]
                    },
                    title: {
                        display: true,
                        text: "Gross Margin"
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                };


            createChart();
            //$interval(createChart, 2000);

            function createChart() {
                $scope.series = [];
                $scope.data = [];
                for (var i = 0; i < 7; i++) {
                    $scope.series.push(`Series ${i}`);
                    $scope.data.push([{
                        x: randomScalingFactor(),
                        y: randomScalingFactor(),
                        r: randomRadius()
                    }]);
                }
            }

            function randomScalingFactor() {
                return (Math.random() > 0.5 ? 1.0 : -1.0) * Math.round(Math.random() * 100);
            }

            function randomRadius() {
                return Math.abs(randomScalingFactor()) / 4;
            }
            hideLoader();
        } catch (exception) {
            alert("Exception: " + exception);
        }
    }

    $scope.plotDoughnutChart = function() {
        try {
            chartFilterParameters.reportid = 8;
            httpService.async().then(function(response) {
                showLoader();

                $scope.chartTitle = response.data.title;
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.options = {
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555",
                        borderColor: ""
                    },
                    /*title: {
                        display: true,
                        text: $data.title
                    },*/
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                };
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoadere();
            })
        } catch (exception) {
            alert("Exception(douhnut): " + exception);
        }
    }


    $scope.plotDynamicChart = function() {
        try {
            chartFilterParameters.reportid = 11;
            httpService.async().then(function(response) {
                showLoader();
                $scope.chartTitle = response.data.chartTitle;
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.type = response.data.type1;
                $scope.options = {
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    title: {
                        display: true,
                        text: response.data.title
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }
                $scope.toggle = function() {
                    $scope.type = $scope.type === response.data.type1 ?
                        response.data.type2 : response.data.type1;
                };
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                hideLoader();
            }, function(error) {
                showLoader();
                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception: " + exception);
        }
    }


    $scope.plotHorizontalBarChart = function() {
        try {
            chartFilterParameters.reportid = 6;
            httpService.async().then(function(response) {
                showLoader();
                $scope.labels = response.data.labels;
                $scope.series = response.data.series;
                $scope.data = response.data.data;
                $scope.options = {
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                              ticks: {
                                callback: function(value, index, values) {
                                    if (parseInt(value) > 999) {
                                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else if (parseInt(value) < -999) {
                                        return '($' + Math.abs(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                    } else {
                                        return '$' + value;
                                    }
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringx,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"
                            }
                        }],
                        yAxes: [{
                            gridLines: {
                                display: true
                            },
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringy,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"

                            }
                          
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    title: {
                        display: true,
                        text: response.data.title
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception:(horizontal) " + exception);
        }
    }

    $scope.plotMixedChart = function() {
        try {
            chartFilterParameters.reportid = 7;
            httpService.async().then(function(response) {
                showLoader();
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.datasetOverride = [{
                    label: response.data.chartLabel1,
                    borderWidth: 1,
                    type: response.data.type1
                }, {
                    label: response.data.chartLabel2,
                    borderWidth: 3,
                    hoverBackgroundColor: "rgba(255,99,132,0.4)",
                    hoverBorderColor: "rgba(255,99,132,1)",
                    type: response.data.type2
                }];
                $scope.options = {
                    scales: {
                        xAxes: [{
                            gridLines: {
                                display: false
                            },
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringy,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"
                            },
                        }],
                        yAxes: [{
                            gridLines: {
                                display: true
                            },
                            ticks: {
                                callback: function(value, index, values) {
                                    if (parseInt(value) > 999) {
                                        return '$' + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
                                    } else if (parseInt(value) < 0) {
                                        return '($' + Math.abs(value).toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ')';
                                    } else {
                                        return '$' + value;
                                    }
                                }
                            },
                            scaleLabel: {
                                display: true,
                                labelString: response.data.labelStringx,
                                fontColor: "#556",
                                fontWeight: "bold",
                                fontSize: "12"
                            },
                        }]
                    },
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    title: {
                        display: true,
                        text: response.data.title
                    },
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }
                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception(mixed): " + exception);
        }
    }


    $scope.plotPieChart = function() {
        try {
            chartFilterParameters.reportid = 4;
            httpService.async().then(function(response) {
                showLoader();
                $scope.chartTitle = response.data.title;
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.colors = ['#45b7cd', '#ff6384', '#ff8e72'];
                $scope.options = {
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    /*title: {
                        display: true,
                        text: $data.title,
                        position: 'bottom'
                    },*/
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }
                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception(pie): " + exception);
        }
    }


    $scope.plotPolarChart = function() {
        try {
            chartFilterParameters.reportid = 10;
            httpService.async().then(function(response) {
                showLoader();
                $scope.chartTitle = response.data.title;
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.options = {
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    /* title: {
                         display: true,
                         text: $data.title
                     },*/
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }

                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception(polar): " + exception);
        }
    }


    $scope.plotRadarChart = function() {
        try {
            chartFilterParameters.reportid = 9;
            httpService.async().then(function(response) {
                showLoader();
                $scope.chartTitle = response.data.title;
                $scope.labels = response.data.labels;
                $scope.data = response.data.data;
                $scope.options = {
                    tooltips: {
                        enabled: true,
                        backgroundColor: "rgb(255, 255, 255)",
                        titleFontColor: "#555",
                        bodyFontColor: "#555"
                    },
                    /*title: {
                        display: true,
                        text: $data.title
                    },*/
                    responsive: $scope.responsive,
                    legend: {
                        display: $scope.showLegend,
                        position: "right",
                        labels: {
                            boxWidth: 12,
                            fontColor: "#777"
                        }


                    }
                }

                hideLoader();
            }, function(error) {

                alert("Error Fetching Data from WebService");
                hideLoader();
            })
        } catch (exception) {
            alert("Exception(radar): " + exception);
        }
    }

})
