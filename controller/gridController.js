 app.controller('BasicGridController', ['$scope', '$http', '$filter','$rootScope', function($scope, $http, $filter,$rootScope) {
     // var today = new Date();
     //store grid API in variable  

     $scope.gridOptions = {
         enableFiltering: false,
         enableSorting: false,
         onRegisterApi: function(gridApi) {
             $scope.gridApi = gridApi;

         },
         columnDefs: [
             { field: 'LocationName', width: 150 },
             { field: 'DoctorName', width: 150 },
             { field: 'TransactionDate', width: 150 },
             { field: 'DOS', width: 150 },
             { field: 'TicketNumber', width: 150 },
             { field: 'Charges', width: 150 },
             { field: 'LagDays', width: 150 }
         ]
     };

     try {
         var url = 'http://192.168.61.132/Controller.aspx';
         var config = {
             method: 'POST',
             url: '../reportCurl.php',
             data: {
                 requestedUrl: url,
                 Servicetype: "GetServiceByLocation",
                 locationids: "",
                 Doctorids: "",
                 fromdate: "",
                 todate: "",
                 LagOperatorType: "",
                 lagdays: "-1",
                 maxlagdays: "-1",
                 type: 3,
                 reportid: 1,
                 dos_doe: 1,
                 weekenddays: 0,
                 CAutoID: 1,
                 SAutoID: 1,
                 orgname: "ICS",
                 length: 1000,
                 start: 0
             }
         };
         var request = $http(config); //created request
         request.then(function(response) {
             showLoader();
             $scope.gridOptions.data = response.data
             hideLoader();
         }, function(error) {
             console.log(error);
         })
     } catch (exception) {
         alert("Exception: " + exception);
     }

     $scope.filterdata = function() {
         return $scope.gridOptions.data;
     }
     $scope.filter = function() {

         $scope.gridApi.grid.registerRowsProcessor($scope.singleFilter, 200);
         $scope.gridApi.grid.refresh();

     };

 }])


 //PAGINATION FROM SERVER
 try {
     app.controller('PaginationGridController', ['$scope', '$http', 'uiGridConstants','$rootScope', function($scope, $http, uiGridConstants,$rootScope) {
         $scope.searchBox = "";
         $scope.pgSize;
         $scope.api = [];
        $rootScope.gridApi = { api: "" };
         var paginationOptions = {
             pageNumber: 1,
             pageSize: 25,
             sort: null
         };

         $scope.gridOptions = {
             paginationPageSizes: [25, 50, 75],
             paginationPageSize: 25,
             useExternalPagination: true,
             enablePaginationControls: false,
             useExternalSorting: true,
             columnDefs: [
                 { name: 'name' },
                 { name: 'gender', enableSorting: false },
                 { name: 'company', enableSorting: false }
             ],

         };

         var getPage = function() {
             var url;
             switch (paginationOptions.sort) {
                 case uiGridConstants.ASC:
                     url = '../../jsons/500_complex.json';
                     break;
                 case uiGridConstants.DESC:
                     url = '../../jsons/500_complex.json';
                     break;
                 default:
                     url = '../../jsons/500_complex.json';
                     break;
             }

             $http.get(url)
                 .success(function(data) {
                     $scope.gridOptions.totalItems = 500;
                     var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
                     $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
                 });
         };

         getPage();

         $scope.setPageSize = function() {
             var data = document.getElementById('pageSz').value;
             alert(data);
             paginationOptions.pageSize = data;
             getPage();
         }

         $scope.gridOptions.onRegisterApi = function(gridApi) {
            $scope.gridApi=gridApi;
             $rootScope.gridApi.api = gridApi;

             $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
                 if (sortColumns.length == 0) {
                     paginationOptions.sort = null;
                 } else {
                     paginationOptions.sort = sortColumns[0].sort.direction;
                 }
                 getPage();
             });
             gridApi.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
                 paginationOptions.pageNumber = newPage;
                 paginationOptions.pageSize = pageSize;
                 getPage();
             });
         }


         $scope.searchPage = function(type) {
            if(type=='web'){           
             var pg = document.getElementById('searchBox').value;
             //for webpage
             if (pg != "") {
                 $scope.gridApi.pagination.seek(pg);
                 //$rootScope.gridApi.api.pagination.seek(pg);
             } else {
                $scope.gridApi.pagination.seek(1);
                 //$rootScope.gridApi.api.pagination.seek(1);
             }
         }else{
             //for mobpage
             var mobpg = document.getElementById('mobsearchBox').value;
                if (mobpg != "") {
                 //$scope.gridApi.pagination.seek(pg);
                 $rootScope.gridApi.api.pagination.seek(mobpg);
             } else {
                // $scope.gridApi.pagination.seek(1);
                 $rootScope.gridApi.api.pagination.seek(1);
             }
         }
     }

     }]);
 } catch (exception) {
     alert("Exception: " + exception);
 }


 /*
 When pagination is enabled, the data is displayed in pages that can be browsed using the built in pagination selector.

<<<<<<< HEAD
 For external pagination, implement the gridApi.pagination.on.paginationChanged callback function. The callback may contain code to update any pagination state variables your application may utilize, e.g. variables containing the pageNumber and pageSize. The REST call used to fetch the data from the server should be called from within this callback. The URL of the call should contain query parameters that will allow the server-side code to have sufficient information to be able to retrieve the specific subset of data that the client requires from the entire set.

 It should also update the $scope.gridOptions.totalItems variable with the total count of rows that exist (but were not all fetched in the REST call mentioned above since they exist in other pages of data).

 This will allow ui-grid to calculate the correct number of pages on the client-side.
 */
=======
//PAGINATION FROM SERVER
app.controller('PaginationGridController', ['$scope', '$http', 'uiGridConstants', function($scope, $http, uiGridConstants) {
  var paginationOptions = {
    pageNumber: 1,
    pageSize: 25,
    sort: null
  };

  $scope.gridOptions = {
    paginationPageSizes: [10, 50, 75],
    paginationPageSize: 10,
    useExternalPagination: true,
     enablePaginationControls: false,
     useExternalSorting: true,
    columnDefs: [
      { name: 'name' },
      { name: 'gender', enableSorting: false },
      { name: 'company', enableSorting: false }
    ],
    onRegisterApi: function(gridApi) {
      $scope.gridApi = gridApi;
      $scope.gridApi.core.on.sortChanged($scope, function(grid, sortColumns) {
        if (sortColumns.length == 0) {
          paginationOptions.sort = null;
        } else {
          paginationOptions.sort = sortColumns[0].sort.direction;
        }
        getPage();
      });
      gridApi.pagination.on.paginationChanged($scope, function (newPage, pageSize) {
        paginationOptions.pageNumber = newPage;
        paginationOptions.pageSize = pageSize;
        getPage();
      });
    }
  };

   var getPage = function() {
    var url;
    switch(paginationOptions.sort) {
      case uiGridConstants.ASC:
        url = '../../jsons/500_complex.json';
        break;
      case uiGridConstants.DESC:
        url = '../../jsons/500_complex.json';
        break;
      default:
        url = '../../jsons/500_complex.json';
        break;
    }
 
    $http.get(url)
    .success(function (data) {
      $scope.gridOptions.totalItems = 100;
      var firstRow = (paginationOptions.pageNumber - 1) * paginationOptions.pageSize;
      $scope.gridOptions.data = data.slice(firstRow, firstRow + paginationOptions.pageSize);
    });
  };
 
  getPage();
}

  ]);


/*
When pagination is enabled, the data is displayed in pages that can be browsed using the built in pagination selector.

For external pagination, implement the gridApi.pagination.on.paginationChanged callback function. The callback may contain code to update any pagination state variables your application may utilize, e.g. variables containing the pageNumber and pageSize. The REST call used to fetch the data from the server should be called from within this callback. The URL of the call should contain query parameters that will allow the server-side code to have sufficient information to be able to retrieve the specific subset of data that the client requires from the entire set.

It should also update the $scope.gridOptions.totalItems variable with the total count of rows that exist (but were not all fetched in the REST call mentioned above since they exist in other pages of data).

This will allow ui-grid to calculate the correct number of pages on the client-side.
*/
>>>>>>> a85996b62f92d469633bf8e2bc48c9ab1b49ec62
