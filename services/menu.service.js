try {
    app.factory('menu', ['$rootScope', '$location', '$http', '$localStorage', function($rootScope, $location, $http, $localStorage) {
        var self;
        return self = {
            sections: $localStorage.menu,
            toggleSelectSection: function(section) {
                self.openedSection = (self.openedSection === section ? null : section);
            },
            isSectionSelected: function(section) {
                return self.openedSection === section;
            },

            selectPage: function(section, page) {
                page && page.url && $location.path(page.url);
                self.currentSection = section;
                self.currentPage = page;
            }
        };

        function sortByHumanName(a, b) {
            return (a.humanName < b.humanName) ? -1 :
                (a.humanName > b.humanName) ? 1 : 0;
        }

    }]);

    //service to make http calls for charts
    app.factory('httpService', ['$http', 'chartFilterParameters', function($http, chartFilterParameters) {
        var httpService = {
            async: function(chartType) {
                // var parameters = chartWebService(chartType);

                // $http returns a promise, which has a then function, which also returns a promise
                var config = {
                    method: 'POST',
                    url: '../reportCurl.php',
                    data: {
                        requester: "ChartCtrl",
                        requestedUrl: 'http://192.168.61.228/Dashboard2.x.8/services/chartService.php',
                        Servicetype: "GetServiceByLocation",
                        locationids: chartFilterParameters.locationids,
                        Doctorids: chartFilterParameters.Doctorids,
                        fromdate: chartFilterParameters.fromdate,
                        todate: chartFilterParameters.todate,
                        LagOperatorType: chartFilterParameters.LagOperatorType,
                        lagdays: chartFilterParameters.lagdays,
                        maxlagdays: chartFilterParameters.maxlagdays,
                        type: chartFilterParameters.type,
                        reportid: chartFilterParameters.reportid,
                        dos_doe: chartFilterParameters.dos_doe,
                        weekenddays: chartFilterParameters.weekenddays,
                        CAutoID: chartFilterParameters.CAutoID,
                        SAutoID: chartFilterParameters.SAutoID,
                        orgname: chartFilterParameters.orgname,
                        length: chartFilterParameters.length,
                        start: chartFilterParameters.start
                    }
                };
                var request = $http(config); //created request
                var promise = request.then(function(response) { //call to 'then' function
                    //$removeXml = response.data.replace('<?xml version="1.0" encoding="utf-8"?>', '');
                    //$removeStartString = $removeXml.replace('<string xmlns="http://tempuri.org/">', '');
                    //$data = angular.fromJson($removeStartString.replace('</string>', ''));
                    //alert(angular.toJson(response));
                    return response;

                }, function(error) {
                    if (error) {
                        alert('Error Fetching Data From Webservice');

                    }
                });
                // Return the promise to the controller
                return promise;
            }
        };
        return httpService;
    }]);


    //service to make http calls for fetching data for filters
    app.factory('getFilterDataService', ['$http', function($http) {
        var getFilterDataService = {
            async: function(sp, location, doctor, orgname) {
                // $http returns a promise, which has a then function, which also returns a promise
                url = "http://192.168.61.132/Controller.aspx"
                var config = {
                    method: 'POST',
                    url: '../dropdowncurl.php',
                    data: {
                        requestedUrl: url,
                        Servicetype: "JSON",
                        insgrpid: "",
                        selectIDs: "-1",
                        rpt: "1",
                        dtrg: "SELECT PERIOD",
                        sp: sp,
                        dos_doe: "1",
                        location: location,
                        doctor: doctor,
                        CAutoID: "1",
                        SAutoID: "1",
                        OrgName: orgname
                    }
                };
                var request = $http(config); //created request
                var promise = request.then(function(response) { //call to 'then' function
                   if(angular.toJson(response.data[0].children)==undefined){
                    alert("Error fetching data for filters from webservice");
                   }else{
                    return angular.toJson(response.data[0].children);
                }

                }, function(error) {
                    alert(angular.toJson(error));
                });
                // Return the promise to the controller
                return promise;
            }
        };
        return getFilterDataService;
    }]);

    //service to use a commong variable in all controllers
    app.factory("isMobileFilter", function() {

        return {
            isMobileFilter: false
        };

    });

    //service for returning json data for charts on filter selection
    app.factory("filterCharts", function() {
        return {
            section: "home.charts.overview",
            id: ""
        };
    });

    //set filterParameters for plotting charts
    app.factory("chartFilterParameters", function() {
        return {
            Servicetype: "GetServiceByLocation",
            locationids: "",
            Doctorids: "",
            fromdate: "",
            todate: "",
            LagOperatorType: "",
            lagdays: "",
            maxlagdays: "",
            type: "",
            reportid: "",
            dos_doe: "",
            weekenddays: 0,
            CAutoID: 1,
            SAutoID: 1,
            orgname: "ICS",
            length: "",
            start: 0
        };
    });
} catch (exception) {
    alert(exception);
}
