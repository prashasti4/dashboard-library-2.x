app.factory("stacktraceService", function() {
                // "printStackTrace" is a global object.
                return({
                    print: printStackTrace
                });
            }
        );

         app.factory("$exceptionHandler",function( $log, $window, stacktraceService,$injector) {
             
                function log( exception, cause ) {
                   
                    $log.error.apply( $log, arguments );
                    
                    try {                     
                      var errorMessage = exception.toString();
                      var stackTrace = stacktraceService.print({ e: exception });
                      var rootScope=$injector.get('$rootScope');
                      var user=rootScope.base64encodedUser;
                      var device=angular.toJson(rootScope.userAgent);
                  //var databaseReference = new Firebase("https://crackling-fire-1440.firebaseio.com/");
                // var databaseReference = new Firebase("https://testproject-cfc64.firebaseio.com/"); 
                var databaseReference = firebase.database().ref();
                 var device=angular.toJson(rootScope.userAgent);
                 if(!user){     
           //  alert(user);
                 var objectReference = databaseReference.child("NotLoggedIn");                 
                      objectReference.push({                     
                      date: Date(),
                      errorUrl: $window.location.href,
                      errorMessage: errorMessage,                          
                      log: [stackTrace]
                      });
                       } 
                      else{   
                    //  alert(user);
                      var objectReference = databaseReference.child(user);
                      objectReference.push({                     
                      date: Date(),
                      device: device,
                        errorUrl: $window.location.href,
                          errorMessage: errorMessage,                          
                          log: [stackTrace]
                      });
                       }                        
                    } catch ( loggingError ) {
                   
                        // For Developers - log the log-failure.
                        $log.warn( "Error logging failed" );
                        $log.log( loggingError );
                    }
                }

                // Return the logging function.
                return( log );
            });