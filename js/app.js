try {
    //declaring module and including dependencies
    var login = angular.module('login', ['ngStorage', 'ui.router', 'ngAnimate', 'ngMaterial', 'ngAria', 'ngMdIcons'])
    var app = angular.module('app', ['ngAnimate', 'ngPopover', 'multiselectDropdown','ui.router', 'ngMaterial', 'ngAria', 'chart.js', 'ngStorage', 'ui.grid', 'ui.grid.pinning', 'ui.grid.edit', 'ui.grid.cellNav', 'ui.grid.grouping', 'ui.grid.exporter',
            'ui.grid.selection', 'ui.grid.infiniteScroll','moment-picker','ui.grid.pagination'
        ])

        //for changing theme
    app.config(function($mdThemingProvider) {
        var customParameters = customizeTheme();
        var customPrimary = customParameters.customPrimary;
        $mdThemingProvider.definePalette('customPrimary',
            customPrimary);
        $mdThemingProvider.theme(customParameters.theme)
            .primaryPalette(customParameters.primaryPalleteColor, {
                'default': customParameters.hue
            })
    })
    login.config(function($mdThemingProvider) {
        var customParameters = customizeTheme();
        var customPrimary = customParameters.customPrimary;
        $mdThemingProvider.definePalette('customPrimary',
            customPrimary);
        $mdThemingProvider.theme(customParameters.theme)
            .primaryPalette(customParameters.primaryPalleteColor, {
                'default': customParameters.hue
            })
    })

    //binding menu from json at app initialization
    app.run(function($log, $http, $localStorage) {
  
      var url = 'http://192.168.61.132/Dashboard2Jsons/menu.json'
           var config = {
               method: 'POST',
               url: '../generalHttpRequest.php',
              headers:{
                'Content-Type':'plain/text',
                'Accept' : 'plain/text'
              },
               data: {
                   requester: "dynamicCtrl",
                   requestedUrl: url                
               }
           };
           var request = $http(config); //created request
           request.then(function(response) { //call to 'then' function  
           // alert(angular.toJson(response));
           if(!response.data.indexOf("Curlerror"))  {        
               alert("Host not available");
             }else{
                $localStorage.menu = response.data;
             }
})
           })


  
  



   



    //take all whitespace out of string
    app.filter('nospace', function() {
        return function(value) {
            return (!value) ? '' : value.replace(/ /g, '');
        };
    })

    //replace uppercase to regular case
    app.filter('humanizeDoc', function() {
        return function(doc) {
            if (!doc) return;
            if (doc.type === 'directive') {
                return doc.name.replace(/([a-z])/g, function($1) {
                    return '-' + $1.toUpperCase();
                });
            }

            return doc.label || doc.name;
        };
    });

/*app.factory('directiveResetApi', function($rootScope) {
    var sharedService = {};

    sharedService.doSomething = function() {
        $rootScope.$broadcast('messageBroadcast');
    };

    return sharedService;
})*/




} catch (exception) {
    alert("Exception(app): " + exception);
}
