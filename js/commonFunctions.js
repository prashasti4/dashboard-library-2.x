function menuBinding() {
    //alert("called");
    var url = { url: 'http://ggcdashboard.pnestec.com/GGCService.asmx/Menu', a: "a", b: "b" };
    return url;
}

function customizeTheme() {

    var customParameters = {
        "theme": "default",
        "primaryPalleteColor": "customPrimary",
        "accentPalette": "purple",
        "warnPalette": "customPrimary",
        "backgroundPalette": "grey",
        "hue": "500",
        "customPrimary": {
            "50": "#0ea7ff",
            "100": "#009bf4",
            "200": "#008ada",
            "300": "#007ac1",
            "400": "#006aa7",
            "500": "#005A8E",
            "600": "#004a74",
            "700": "#003a5b",
            "800": "#002a41",
            "900": "#001928",
            "A100": "#28b0ff",
            "A200": "#41baff",
            "A400": "#5bc3ff",
            "A700": "#00090e"
        }
    }


    return customParameters;
}

function getLinkTemplateUrl(type) {
    if (type === 'link') {
        menuLinkHtml = '<md-button ng-class="{\'{{section.icon}}\' : true}" ui-sref-active="active" \n' + 'ui-sref="{{section.state}}" ng-click="callWebService(section.state,section.id); ">\n' +
            '  {{section | humanizeDoc}}\n' + /*'<md-icon md-font-set="{{section.icon}}" aria-hidden="true" class="logoutIcon" ></md-icon>' + */'</md-button>\n' + ''
        return menuLinkHtml;
    }
    if (type === 'toggle') {
        menuToggleHtml = '<md-button class="md-button-toggle" ng-class="{\'toggled\' : isOpen(),\'{{section.icon}}\':true}"\n' +
            '  ng-click="toggle()"\n' +
            '  aria-controls="docs-menu-{{section.name | nospace}}"\n' +
            '  flex layout="row"\n' +
            '  aria-expanded="{{ isOpen() }}">\n' +
            '  {{section.name}}\n' +
            '  <md-icon md-font-set="fa fa-chevron-up" aria-hidden="true" class="md-toggle-icon" ng-class="{\'toggled\' : isOpen()}" style="float:right !important;"></md-icon>' +
            '</md-button>\n' +
            '<ul ng-show="isOpen()" id="docs-menu-{{section.name | nospace}}" class="menu-toggle-list">\n' +
            '  <li ng-repeat="page in section.pages">\n' +
            '    <menu-link section="page"></menu-link>\n' +
            '  </li>\n' +
            '</ul>\n' +
            ''
        return menuToggleHtml;
    }
}

function chartWebService(chartType) {
    switch (chartType) {
        case "line":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/LineChart', a: "a", b: "b" };
            return parameters;

        case "bar":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/BarChart', a: "a", b: "b" };
            return parameters;

        case "pie":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/PieChart', a: "a", b: "b" };
            return parameters;

        case "bubble":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/BubbleChart', a: "a", b: "b" };
            //http://192.168.61.132/GGCService.asmx/BubbleChart
            return parameters;

        case "doughnut":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/DoughnutChart', a: "a", b: "b" };
            return parameters;

        case "dynamic":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/DynamicChart', a: "a", b: "b" };
            return parameters;

        case "horizontalBar":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/HorizontalBarChart', a: "a", b: "b" };
            return parameters;

        case "mixed":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/MixedChart', a: "a", b: "b" };
            return parameters;

        case "polar":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/PolarChart', a: "a", b: "b" };
            return parameters;

        case "radar":
            var parameters = { url: 'http://192.168.61.132/GGCService.asmx/RadarChart', a: "a", b: "b" };
            return parameters;
    }
}

function setAlignment() {  
    var windowWidth = window.innerWidth;  
  var innerWidth=windowWidth-2;    
    var marginLeft=windowWidth-(windowWidth*(82/100))+'px';
    //var minWidth=windowWidth-50+'px'; 
    angular.element(document.querySelector(".innerView")).css('width', windowWidth);
   angular.element(document.querySelector("#indexView")).css('minWidth', innerWidth);
   //alert(headerHeight);
}



function showLoader() {
    document.getElementById('divmask').style.display = "block";
    document.getElementById('divloading').style.display = "block";
}

function hideLoader() {
    document.getElementById('divmask').style.display = "none";
    document.getElementById('divloading').style.display = "none";
}

function positionChartTitle(){
    var canvasBaseWidth=document.getElementById('canvasBase').style.width;
    canvasBaseWidth=canvasBaseWidth.replace('%','');
    var position=canvasBaseWidth/2;
    //angular.element(document.querySelector("#chartTitle")).css('marginLeft', position);
    document.getElementById("chartTitle").style.marginLeft=position;
}


<<<<<<< HEAD
function isWeb() {
    var windowWidth = window.innerWidth;
    if (windowWidth >= 729) {
        console.log("web");
        return true;
    } else {
        console.log("mob");
canvas = document.getElementById('canvas');
        canvas.width = window.innerWidth; 
        canvas.height = window.innerWidth - 20;
        return false;
    }
}

=======
>>>>>>> a85996b62f92d469633bf8e2bc48c9ab1b49ec62
